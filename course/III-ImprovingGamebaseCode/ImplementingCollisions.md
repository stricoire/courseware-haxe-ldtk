---
title: Implementing Collisions
nav_order: 10
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- It's great to actually render our level using `LDtk` but will it not be greater if you could use this layout as the base for our collision system?
<br>


## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Identify** in which collision situation we are <br>
⬜ **Say** for a given cell if there is a collision using `LDtk` layout <br>
⬜ **Determine** if a given Entity is on the ground <br>
⬜ **Adjust** if needed the collision sensibility

<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Get collisions information from LDtk
{: .highlight-stiple}

<center> Inside folder <code>src/</code> 📁 : </center>

- **Create** a new file named `Types.hx`.
```js
// Will permit to identify which situation/collision occured
// and for eg. later on to adapt animation of Entities accordingly
enum LevelMark {
    Grab;
    GrabLeft;
    GrabRight;
    PlatformEnd;
    PlatformEndLeft;
    PlatformEndRight;
}
```

<br>
<center> Inside <code>import.hx</code> 📄 : </center>

- **Add** the previously created file :
```js
import Types;
```

<br>
<center> Inside <code>Level.hx</code> 📄 : </center>

- **Replace** existing class variables with the following (*which uses `LDtk` and therefore will get automatically the correct values*) :
```js
// Get respectively the number of cells in lenght and in height
// It will be usefull to determine in which cell we are
public var cWid(get,never) : Int; inline function get_cWid() return level.l_Collisions.cWid;
public var cHei(get,never) : Int; inline function get_cHei() return level.l_Collisions.cHei;
```

<br>
- **Add** new class variable :
```js
  // Given a LevelMark (for eg : "Grab" or "PlatformEndRight") it will return
  // a map containing for a given cell numero, a boolean which indicate
  // if there is a collision or not
  var marks : Map< LevelMark, Map<Int,Bool> > = new Map();
```

<br>
- **Add** a new function :
```js
/** Return TRUE if "Collisions" layer (the same we defined in LDtk previously)
contains a collision value : aka a "1" because it corresponds to wall inside LDtk **/
public inline function hasCollision(cx,cy) : Bool {
    return !isValid(cx,cy) ? true : level.l_Collisions.getInt(cx,cy)==1;
}
```

<br>
✅ **Identify** in which collision situation we are <br>
✅ **Say** for a given cell if there is a collision using `LDtk` layout

<br>
### Get collisions information from LDtk
{: .highlight-stiple}

<center> Inside <code>Entity.hx</code> 📄 : </center>

- **Add** new class variables:
```js
/* This return TRUE if the actual entity is on the ground.
It's TRUE if the actual velocity on y equal 0 or if there is a collision
just under the entity (aka cy+1)  */
public var onGround(get,never): Bool;
```
```js
inline function get_onGround() return dy==0 && level.hasCollision(cx,cy+1) && yr==1;
```
```js
// In case we implement climbing later on
public var climbing = false;
```

<br>
- **Add** in `update()` function the following, (*just under the already existing comments*) :
```js
// [ add X collisions checks here ]
if( level.hasCollision(cx+1,cy) && xr>=0.7 ) {
    xr = 0.7;
    dx *= Math.pow(0.5,tmod);
}
if( level.hasCollision(cx-1,cy) && xr<=0.3 ) {
    xr = 0.3;
    dx *= Math.pow(0.5,tmod);
}
```
```js
// [ add Y collisions checks here ]
if( !climbing && level.hasCollision(cx,cy-1) && yr<0.5 ) {
    yr = 0.5;
    dy *= Math.pow(0.5,tmod);
}
if( level.hasCollision(cx,cy+1) && yr>=1 ) {
    dy = 0;
    yr = 1;
}
```

> ℹ️ : If you want more details about these collision checks, you should have a look on the great tutorial `deepnight` did about it [here](https://deepnight.net/tutorial/a-simple-platformer-engine-part-1-basics/)

<br>
✅ **Determine** if a given Entity is on the ground

<br>
### Adjust collisions system to your need
{: .highlight-stiple}

- Your collision system with the level should already work by now !
You may have noticed that your character enters slightly into the ceiling, this can be adjusted through the following lines of code (*Still in `update()` in `Entity.hx`*):
```js
if( !climbing && level.hasCollision(cx,cy-1) && yr<0.5 ) {
    yr = 0.5;
    dy *= Math.pow(0.5,tmod);
}
```

> ℹ️ : What `yr` defined here is the **relative position** of your character inside a cell
*(range from 0 to 1; value 0 your are at the top of the cell; value of 1 at the bottom)*.

> ℹ️ : So here, what it says is that, if there is a collision above you (cy - 1)
AND the relative position of your character is yr < 0.5 *(aka you're above
the half upper limit of the cell)* you will be keeped at this position.
Thus if you want to limit the inserting of your character into the ceiling you should increase the value of `yr`.

<br>
✅ **Adjust** if needed the collision sensibility

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have a working collisions system
</div>
