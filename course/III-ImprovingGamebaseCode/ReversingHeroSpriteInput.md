---
title: Reversing hero sprite based on inputs
nav_order: 8
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Find** the direction of our sprite <br>
⬜ **Update** it based on inputs

<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Reverse hero sprite based on inputs
{: .highlight-stiple}

<center> Inside <code>Hero.hx</code> 📄 : </center>

- **Add** the following lines of code in the function `update` :
```js
    // Update the direction of player
    // -> will allow to reverse the sprite accordingly
    // -> See : Entity.hx -> postUpdate() -> spr.scaleX = dir*sprScaleX;
    if( !controlsLocked() && ca.leftDist() > 0 ){
      var xPow = Math.cos( ca.leftAngle() );
      dir = M.fabs(xPow)>=0.1 ? M.sign(xPow) : dir;
    }
```

> ℹ️ : Basically what this function does is updating the sign of the variable `dir` (aka : *direction*),
based on the input you pressed : you go right, `dir` sign will be positive, and if you go left it will be negative. So when calculating the new `spr.scaleX = dir*sprScaleX`, it will be reverse if you go left due to the negative sign of `dir` variable (*which is totally what we want*).

> ⚠️ : If you compile right away, your computer will not be happy telling you that `controlsLocked` function doesn't exist, which is true. Let's add it !

<br>
<center> Inside <code>Entity.hx</code> (<i>which is the parent class of <code>Hero.hx</code></i>)  📄 : </center>

- **Add** the following function :
```js
public inline function controlsLocked() {
    return destroyed || cd.has("ctrlLocked");
}
```

> ℹ️ : This part is a bit optionnal, as this function only allow us to setup a cooldown on the controllers input, to prevent it from working on determined occasion (*for eg : The character is hit by an enemy and you want 0.5 sec period after it, during which player can't move the character*)

> ⚠️ : If you don't want to implement this function, make sure to remove it whenever it's called later on, in the code

<br>
✅ **Find** the direction of our sprite <br>
✅ **Update** it based on inputs


<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have your <code>Hero</code> moving accordingly to the direction you input
</div>
