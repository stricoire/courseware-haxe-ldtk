---
title: Add Textbox
nav_order: 13
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- If you make a game, you will probably need some text to give informations to your players. <br>
Let's see how we can defined it into `LDtk` and then integrate it into our game via code. <br>
Obviously we will add some nice apparition effect and adaptative color scheme.
<br>


## 📝 What we need to do
{:toc .fs-7}

---

<u> In <code>LDtk</code>: </u><br>
&nbsp;&nbsp;&nbsp; ⬜ **Create** a new `LDtk` Entity `Text` with its specific parameters (*especially its text*) <br>
&nbsp;&nbsp;&nbsp; ⬜ **Create** a new `LDtk` Entity `Trigger` with its specific parameters (*especially its triggering radius*) <br>
&nbsp;&nbsp;&nbsp; ⬜ **Add** `Text` and `Trigger` to our level <br>

<u> Through code: </u><br>
&nbsp;&nbsp;&nbsp; ⬜ **Create** a new class for our `Text` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Get** all the data we defined in `LDtk` <br>
&nbsp;&nbsp;&nbsp; ⬜ **Create** a new class for our `Trigger` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Develop** a function which determine the effect of triggering <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Link** triggering effect to text <br>
&nbsp;&nbsp;&nbsp; ⬜ **Instantiate** `Text` and `Trigger`


<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Define Text and Trigger Entity in LDtk
{: .highlight-stiple}

- **Open** your `LDtk` project

- **Create** a new Entity called `Text`

- **Go** inside the `Text` panel entity

- **Click** on the following `+FIELD` button and add : <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **String** named `text`. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **Color** named `color` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **Boolean** named `startHidden` (*as its name implies, it will be usefull if we want to hide some text but to show it under certain circumstances, for eg : trigger*)

It should normaly looks like the following :

<figure>
  <img src="{{site.baseurl}}/attached_files/images/TextEntity_LDtk.png"
       width="500">
</figure>

<br>
- **Create** a new Entity called `Trigger`

- **Go** inside inside the `Trigger` panel entity

- **Click** on the following `+FIELD` button and add : <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **Point** named `target` (*this will be the point where we want our text to be spawning*) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **Boolean** named `hidden` (*as its name implies, it will be usefull if we want to have some invisible trigger*) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • An **Integer** named `radius` (*this will defined the range of detection of our trigger*) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **Boolean** name `exitLevel` (*will be usefull later on to detect end of level and act accordingly*)

It should normaly looks like the following :

<figure>
  <img src="{{site.baseurl}}/attached_files/images/TriggerEntity_LDtk.png"
       width="500">
</figure>

<br>
- **Place** wherever you want some texts (*hidden or not*), but keep in mind that if hidden, you should trigger hit latter on.
> ℹ️ : To associate trigger to text, just put the `target` point of trigger to the place where you want your text to spawn, and place your text just next to it (*trigger can be hide or not, and have a radius equal to 0 or bigger*).

<figure>
  <img src="{{site.baseurl}}/attached_files/images/TextAndTriggers_LDtk.png"
       width="500">
</figure>


- That's all we need concerning `LDtk`, let's go back to code !

<br>
✅ **Create** a new `LDtk` Entity `Text` with its specific parameters (*especially its text*) <br>
✅ **Create** a new `LDtk` Entity `Trigger` with its specific parameters (*especially its triggering radius*) <br>
✅ **Add** `Text` and `Trigger` to our level <br>

<br>
### Define Text class
{: .highlight-stiple}

<center> Inside folder <code>src/en/</code> 📁 : </center>

- **Create** a new *file* `Text.hx` :

```js
package en;

class Text extends Entity {
  public static var ALL : Array<Text> = [];

  var data : Entity_Text;
  var wrapper : h2d.Object;
  var bg : h2d.ScaleGrid;
  var tf : h2d.Text;
  public var textVisible = true;

  public function new(e:World.Entity_Text) {
    super(e.cx, e.cy);
    data = e;
    ALL.push(this);
    gravityMul = 0;
    // We defined visibility of our text based on what we defined in LDtk
    textVisible = !data.f_startHidden;

    wrapper = new h2d.Object();
    game.scroller.add(wrapper, Const.DP_BG);

    // We set text color based on what we defined in LDtk
    var c = e.f_color_int;
    // We defined the border around our text (px for left/right border ,py for top/down border)
    var px = 8;
    var py = 6;
    // ScaleGrid is interessting as it allow us to render a Tile as a stretchable surface with unscaled corners
    // Here left/right corners are 5 pixels wide; and the same for top/bottom corners
    bg = new h2d.ScaleGrid(Assets.tiles.getTile("uiBox"), 5, 5, wrapper);
    tf = new h2d.Text(Assets.fontPixel, wrapper);
    // We start our text at (px,py) pixels
    tf.setPosition(px,py);
    // We get our text from LDtk
    tf.text = e.f_text;
    tf.textColor = c;
    tf.maxWidth = 160;

    // We define the size of our uiBox based on borders + text
    bg.width = px*2 + tf.textWidth;
    bg.height = py*2 + tf.textHeight;
    bg.color.setColor( C.addAlphaF( C.interpolateInt(c, 0x322445, 0.8) ) );
    bg.colorAdd = new h3d.Vector();
  }
}
```

> ℹ️ : You may have noticed this line `bg.color.setColor( C.addAlphaF( C.interpolateInt(c, 0x322445, 0.8) ) );`
and doesn't understand it at first sight (*which is completly normal*).
This function set the **BackgroundColor of our uiBox** based on our text color and another color, making interpolation between the two.
Let's dig into how it works concretly.
- First there is `interpolateInt` **function** which takes two int colors and a ratio and make a linear interpolation between the two based on the ratio you give. If ratio is 0, it will only used the first color; if 1, it will only used the second color; if 0.5 it will takes a color in the middle of the two.
- Then there is `addAlphaF`. You may have noticed that with the color **0x322445** (*which defines a violet*) there is no alpha information (*only 24 bits compared to 32 bits expected; or in simpler terms, 6 alphanumerics after 0x instead of 8*), so this function is used to add alpha value to it.
Here as it's unspecified, alpha by default will equal 1, which means that our uiBox will be completly opaque (*try to add 0.5, and you'll see that your uiBox is now a bit transparent*)

<br>
- **Add** the following to `Text.hx`:

```js
override function dispose() {
  super.dispose();
  ALL.remove(this);
  wrapper.remove();
}

public function reveal() {
  textVisible = true;
  //blink(0xffcc00);
  cd.setS("revealAnim",0.5);
}

override function postUpdate() {
  super.postUpdate();
  spr.visible = false;
  wrapper.visible = textVisible;
  //bg.colorAdd.load( blinkColor );
  wrapper.x = Std.int( data.pixelX - bg.width*0.5 );
  // This line is very interesting as it creates little moving animation
  // when text is revealed based on the cd "revealAnim"
  wrapper.y = Std.int( data.pixelY - bg.height*0.5 ) + Std.int(8*cd.getRatio("revealAnim"));
}

override function update() {
  super.update();
  //if( distCase(hero)<=12 )
  //  cd.setS("playerWasAround",Const.INFINITE);
}
```

<br>
<center> Inside <code>Entity.hx</code> 📄 : </center>

- **Add** new class variable :
```js
	public var hero(get,never) : en.Hero; inline function get_hero() return Game.ME.hero;
```
> ℹ️ : This will allow us to access `Hero` Entity in other classes (*for eg: `Trigger`, to check if hero is triggering it or not*)

<br>

- **Add** two new functions which will give us the number of cell separating two entities:
```js
public inline function distCaseX(e:Entity) return M.fabs( (cx+xr) - (e.cx+e.xr) );
public inline function distCaseY(e:Entity) return M.fabs( (cy+yr) - (e.cy+e.yr) );
```

<br>
✅ **Create** a new class for our `Text` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Get** all the data we defined in `LDtk`

<br>
### Define Trigger class
{: .highlight-stiple}

<center> Inside folder <code>src/en/</code> 📁 : </center>

- **Create** a new file `Trigger.hx` :

```js
package en;

class Trigger extends Entity {
  public static var ALL : Array<Trigger> = [];
  var data : Entity_Trigger;
  var triggered = false;

  public function new(e:Entity_Trigger) {
    super(e.cx, e.cy);
    gravityMul = 0;
    ALL.push(this);
    data = e;
    game.scroller.add(spr, Const.DP_BG);

    spr.set("triggerOff");
  }

  public function trigger() {
    if( triggered )
      return false;

    triggered = true;

    if( data.f_exitLevel ) {
      // Should go to next level, but for now do nothing
      return true;
    }

        // Here we detect if among all the existing text in the level,
        // there are some next to target point and if so, we just reveal them
    if( data.f_target!=null ) {
      for(e in en.Text.ALL)
        if( !e.textVisible && e.distCaseFree(data.f_target.cx, data.f_target.cy)<=2 )
          e.reveal();
    }

    return true;
  }

  override function postUpdate() {
    super.postUpdate();
        // If our trigger is defined "hidden" in LDtk we hide its sprite
    if( data.f_hidden )
      spr.visible = false;

        // We change trigger sprite based on its status
    if( triggered && spr.groupName!="triggerOn" )
      spr.set("triggerOn");

    if( !triggered && spr.groupName!="triggerOff" )
      spr.set("triggerOff");
  }

  override function dispose() {
    super.dispose();
    ALL.remove(this);
  }

  override function update() {
    super.update();

        // If hero is inside the radius of trigger / or directly on it, we should activate it
        if( data.f_radius==0 && hero.onGround && hero.distCaseX(this)<=1 && hero.cy==cy )
            trigger();
        else if( data.f_radius>0 && hero.distCase(this)<=data.f_radius+0.5 )
            trigger();
  }
}
```

<br>
✅ **Create** a new class for our `Trigger` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Develop** a function which determine the effect of triggering <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Link** triggering effect to text <br>

<br>
### Instantiate Text and Trigger
{: .highlight-stiple}

<center> Inside <code>Level.hx</code> 📄 : </center>

- **Add** in `attachMainEntities()`:

```js
for(e in level.l_Entities.all_Text)
  new en.Text(e);

for(e in level.l_Entities.all_Trigger)
  new en.Trigger(e);
```

<br>
✅ **Instantiate** `Text` and `Trigger`


<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have textbox appearing with smooth effect !
</div>

<br>
### Optionnal) Change text font
{: .highlight-stiple .text-align-right}

- Maybe you want to add your own font to the project, if so, you will need to give attention to this line :
`tf = new h2d.Text(Assets.fontPixel, wrapper);` inside `Text.hx`.

- Indeed if you go to `Assets.hx` you will noticed that the variable `fontPixel` is defined and load the corresponding font,
which is an `.fnt` file. After doing my research, Haxe doesn't seem to be able to load directly `.otf` file (*which is a file format widely spread*).

- Adding your own font is in fact a bit tricky but nor impossible.
If you don't have your font in bitmap format (*aka : .fnt + .png*) you will need to convert it.

<br>
#### 📝 What we need to do
{: .fs-5}

---

⬜ **Convert** our font into bitmap format <br>
⬜ **Load** it trough code <br>

<br>
#### 💻 Let's do this !
{: .fs-5}

---

- **Download** [`BMFont`](http://www.angelcode.com/products/bmfont/), which is a bitmap converter.

- **Open** `BMFont`

- **Select** `Options → Font settings` : <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Choose** your font using `Font` button <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Change** the size using `Size (px)` button <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Check** `Render from TrueType outline` if you want smooth fonts <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Click** `OK` to validate <br>

<br>
- **Select** `Options → Export Options` : <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Set** `Bit depth` to "32". <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • In dropdown menu `Preset`, **Select** "white text with alpha" (*black or outline w/ alpha will do as well, but black isn't tintable*) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • In dropdown menu `Texture` make sure to **Select** "Png" <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Click** `OK` to validate

<br>
- **Make sure** to have `Latin` checked as followed, otherwise nothing will be outputed:

<figure>
  <img src="{{site.baseurl}}/attached_files/images/BitmapFontGenerator.png"
       width="600">
</figure>

<br>
- **Select** `Options → Save bitmap font as..` : <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • Put in your Heaps project under `./res/fonts/myFontName.fnt`
> ⚠️ Note: Multi-texture fnt files are not supported, ensure that there is only one font texture per fnt file.


<br>
<center> Inside <code>Assets.hx</code> 📄 : </center>
- **Modify** the following line accordingly : <br>
`fontPixel = hxd.Res.fonts.`+_nameOfYourFont_+`.toFont();`.

<br>
✅ **Convert** our font into bitmap format <br>
✅ **Load** it trough code

Should work now 😊 (*if you want more info concerning the gestion of fonts by `Heaps` feel free to check [this](https://heaps.io/documentation/text.html)*)


<br>
### Optionnal) Add blinking effect to text when revealed
{: .highlight-stiple .text-align-right}

#### 📝 What we need to do
{: .fs-5}

---


⬜ **Create** a new color variable representing our blink inside `Entity.hx` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Specified** its value through a dedicated function <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Make** it slowly invisible as time goes on <br>
⬜ **Call** the dedicated function inside `Text.hx`

<br>
#### 💻 Let's do this !
{: .fs-5}

---

<center> Inside <code>Entity.hx</code> 📄 : </center>

- **Add** new class variable :
```js
public var blinkColor : h3d.Vector;
```

<br>

- **Add** in `new` function :
```js
blinkColor = new h3d.Vector();
```

<br>

- **Add** in `dispose()` :
```js
blinkColor = null;
```

<br>

- **Add** a new function `blink()`:
```bash
public function blink(c:UInt, ?timeInSec=0.06) {
	blinkColor.setColor(c);
	cd.setS("keepBlink",timeInSec);
}
```

<br>

- **Add** in `postUpdate()` function :
```js
// Blink fade
if( !cd.has("keepBlink") ) {
	blinkColor.r*=Math.pow(0.60, tmod);
	blinkColor.g*=Math.pow(0.55, tmod);
	blinkColor.b*=Math.pow(0.50, tmod);
}
spr.colorAdd.load(blinkColor);
```

<br>
<center> Inside <code>Text.hx</code> 📄 : </center>

- **Uncomment** the line where `blink` is mentionned.

<br>
✅ **Create** a new color variable representing our blink inside `Entity.hx` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Specified** its value through a dedicated function <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Make** it slowly invisible as time goes on <br>
✅ **Call** the dedicated function inside `Text.hx`

<br>
<br>

Et voilà, you should now have your text blinking for a fraction of second when it's revealed 😊

> ℹ️ : If you want to increase the duration of the blink, just give a second parameter to blink function when you called it inside `Text.hx`. By default when nothing is specified, the effect last 0.6 seconds. <br>
> ℹ️ : This blinking effect could be used now for all entities, for eg: *when player land after an important fall, you could easily, using this function add a blinking red effect on the sprite*.
