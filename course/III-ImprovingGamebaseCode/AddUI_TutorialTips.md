---
title: UI - Tutorial tips
nav_order: 15
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- Telling your players how to play is very important, let's implement tutorial tips !
<br>


## 📝 What we need to do
{:toc .fs-7}

---

&nbsp;&nbsp;&nbsp; ⬜ **Create** a new class for our `TutorialTips` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Design** it through code <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Define** juicy effect when it appears <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Make sure** to position it correctly depending on the window size <br>

&nbsp;&nbsp;&nbsp; ⬜ **Instantiate** it when needed (*for eg: At start of level, or when player die*)  <br>


<br>

## 💻 Let's do this !
{:toc .fs-7}

---

<br>
### Define Tutorial tips class
{: .highlight-stiple}

<center> Inside folder <code>src/ui/</code> 📁 : </center>

- **Create** a new *file* `TutorialTip.hx` :

```js
package ui;

import h2d.ScaleGrid;

class TutorialTip extends dn.Process {
  public static var ME : TutorialTip;
  var flow : h2d.Flow;
  var cAdd : h3d.Vector;
  public var tutoId : String;

  public function new(tutoId:String, txt:String) {
    super(Game.ME);
    if( ME!=null )
      ME.close();

    cAdd = new h3d.Vector();
    this.tutoId = tutoId;
    ME = this;
    createRootInLayers(Game.ME.root, Const.DP_UI);

    flow = new h2d.Flow(root);
    //Setting a background tile will create an h2d.ScaleGrid background
    //which uses the Flow.borderWidth/Flow.borderHeigh values for its borders.
    flow.backgroundTile = Assets.tiles.getTile("uiBox");
    flow.borderWidth = flow.borderHeight = 8;
    flow.layout = Vertical;
    flow.verticalSpacing = 4;
    flow.horizontalAlign = Middle;
    // We define the padding for each side to correctly position our text
    flow.paddingBottom = 10;
    flow.paddingLeft = 10;
    flow.paddingRight = 10;
    flow.paddingTop = 8;

    var tf = new h2d.Text(Assets.fontPixel,flow);
    tf.text = "Tutorial";
    tf.textColor = 0xFFB300;
    tf.colorAdd = cAdd;
    tf.setPosition(10,10);

    var tf = new h2d.Text(Assets.fontPixel,flow);
    tf.text = txt;
    tf.textColor = 0x15FFA8;
    tf.maxWidth = 150;

    //Make the window appears in motion
    tw.createS(flow.y, 100>0, TElasticEnd, 0.3);
    for(i in 0...8)
      //Make the text blink with a small delay in between
      delayer.addS(blink, 0.5+i*0.33);
  }
  function blink() {
    cAdd.r = 0.9;
    cAdd.g = 0.9;
    cAdd.b = 0.9;
  }
  public function close() {
    if( ME==this )
      ME = null;
    //Make the window disappears in motion
    tw.createS(flow.y, -100, 0.2).end( destroy );
  }
  override public function onDispose() {
    super.onDispose();
    if( ME==this )
      ME = null;
  }
  override public function postUpdate() {
    super.postUpdate();
  }
  override public function update() {
    super.update();
    cAdd.r*=0.8;
    cAdd.g*=0.8;
    cAdd.b*=0.8;
  }

  override function onResize() {
    super.onResize();
    root.setScale(Const.UI_SCALE);
    root.x = w() - flow.outerWidth*Const.UI_SCALE - flow.outerWidth*Const.UI_SCALE /65;
  }
}
```

<br>
✅ **Create** a new class for our `TutorialTips` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Design** it through code <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Define** juicy effect when it appears <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Make sure** to position it correctly depending on the window size <br>

<br>
### Instantiate Tutorial tips when needed
{: .highlight-stiple}

<center> Inside <code>Game.hx</code> 📄 : </center>

- **Add** the following, in function `startLevel()` :
```js
// If it's level 0 we Instantiate our TutorialTipUi and give some tips on how to jump
if (idx == 0){
    new ui.TutorialTip("JumpIndication", "This is level 0. You can Jump using UP ARROW");
}
```

<br>

- **Add** the following, inside `update()` function, inside `if( !ui.Console.ME.isActive() && !ui.Modal.hasAny() )` condition :
```js
// If player press Up we make the jump indication disappear
if(ca.isKeyboardPressed(Key.UP)){
    if( ui.TutorialTip.ME!=null && ui.TutorialTip.ME.tutoId=="JumpIndication" ) {
      cd.setS("lock",0.7);
      ui.TutorialTip.ME.close();
    }
}
```

<br>

- **Add** the following, inside `update()` function, inside `if(ca.selectPressed())` condition :
```js
// If player press R we make the restart indication disappear
if( ui.TutorialTip.ME!=null && ui.TutorialTip.ME.tutoId=="RestartTips" ) {
    cd.setS("lock",0.7);
    ui.TutorialTip.ME.close();
}
```

<center> Inside <code>Hero.hx</code> 📄 : </center>

- **Add** the following inside `hit()` function just under `//Will add stuff concerning restart later on` comment :

```js
/* stuff */

cd.setS("ctrlLocked", 999999);
// Allow to delay the call of the function
game.delayer.addS(function() {
 new ui.TutorialTip("RestartTips", "Press R to restart");
},1.2);

/* stuff */
```

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/TutorialTips.png"
       width="500">
</figure>

<br>
&nbsp;&nbsp;&nbsp; ✅ **Instantiate** it when needed (*for eg: At start of level, or when player die*)  

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have juicy Tips Windows on the top right of your screen 😊
</div>
