---
title: Improving Gamebase code
nav_order: 7
has_children: true
---

## Gamebase code
{: .fs-7}

---

## About the this part

The purpose of this part is to give you little code snippets to increase your understanding of the logic behind `Haxe` and `LDtk`. I'll rely mainly on jams games realized by the developer [deepnight](https://deepnight.net/).

Each sub part will be the occasion to try to extract and understand the structure of his code, so that we'll be able to reproduce in the long run similar functionalities in our own projects (*for eg: to manage
the animations of our entities, to implement a management of the collisions, to create the typical movements of a 2D platformer,
to add text, etc*).

### Special Thanks

Big thanks to `deepnight` taking the time with GameBase and LDtk, it helps a lot ! 👍

### Disclaimer

I'm a junior programmer, who relies mainly on his academic background to deconstruct the code, and propose functional code snippets.
Please, keep in mind though that it is possible that I made mistakes or that I proposed not fully optimized solutions. Feel free to tell me so, and I'll try to correct my mystakes !
