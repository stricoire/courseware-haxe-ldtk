---
title: Add Gravity . . .
nav_order: 11
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- You may have notice that our character is actually flying in the air, although it is possible in the context of a superman game
we would prefer here, that he is subjected to gravity and that he can jump.
<br>


## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Create** a gravity constant <br>
⬜ **Use** this constant to modify our Y velocity when we are falling <br>

<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Setup gravity
{: .highlight-stiple}

<center> Inside <code>Const.hx</code> 📄 : </center>

- **Add** a new variable:
```js
// This value is totally adjustable so experiment with it and have fun
public static var GRAVITY = 0.028;
```

<br>
<center> Inside <code>Entity.hx</code> 📄 : </center>

- **Add** new class variable:
```js
public var gravityMul = 1.0;
```

<br>
- **Add**, in `udpate()` function *(just under `//Y` and above `var steps = M.ceil( M.fabs(dyTotal*tmod) );`)*
```js
if( !onGround && !climbing)
    dy += gravityMul*Const.GRAVITY * tmod;
```
> ℹ️ : It adds velocity in Y when you aren't on the ground *(nor climbing, but for now it doesn't matter)*.

<br>
✅ **Create** a gravity constant <br>
✅ **Use** this constant to modify our Y velocity when we are falling

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have a gravity system affecting your character
</div>
