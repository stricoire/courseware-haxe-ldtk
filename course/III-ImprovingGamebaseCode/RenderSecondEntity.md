---
title: Rendering a second animated Entity
nav_order: 9
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- Supposedly using [`tiles.png`]({{site.baseurl}}/attached_files/files/tiles.png) of this tutorial, you may have noticed that it contains the sprite of a knight. Also our current level, feel a bit empty for our Hero, so let's add him some friends.
<br>


## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Create** a new `LDtk` Entity named `Knight` with its specific parameters (*especially its path*) <br>
⬜ **Add** `Knight` to our level <br>
⬜ **Create** a new class for our `Knight` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Get** the animation from atlas <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Modify** the sprite to add some juice <br>
⬜ **Instantiate** `Knight` create its instance in the game

<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Setup Knight in LDtk
{: .highlight-stiple}

- **Open** `world.ldtk` project

- **Create** a new Entity called `Knight` (*the same way we did in `LDtk/Create First Project`*)

Now we will see an unseen functionnality of `LDtk` software : it's capacity to describe Game design elements directly in it, and thus link these elements to specific Entities.

> ℹ️ : For eg: You could specify how many life points each of your Entities have at start of level, or what items is inside a specific chess, etc.

In our case, we will describe the patrol path of each our Knight, so that afterwards we can make them move in an easy but autonomous way via code.

- **Go** inside inside the `Knight` panel entity

- **Click** on the following `+FIELD` button and add : <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • A **Point variable** named `patrol` (*inside `field Identifier`*). <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • An **Integer** name  `initialDir`: Set `Default Value` to *1* and `Limits` respectively to *-1* and *1*.

It should look like this :

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/KnightEntity_LDtk.png"
       width="600">
</figure>

Now for each `Knight` you put inside your level, the following window should appear recapitulating the information related to this specific Knight.

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/KnightEntity_patrolPath_LDtk.png"
       width="500">
</figure>

- Place as much knights as you want, specifying his patrol path.
For me, as I placed several `Knight`, it looked like this :

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/Knights_LDtk.png"
       width="500">
</figure>

> ℹ️ : The path followed by each knight is represented by point and dot line. By default it's hidden. To display it, select the `patrol` variable inside `Knight` panel entity and select *Show point* inside `Display in editor` field.

<br>
✅ **Create** a new `LDtk` Entity named `Knight` with its specific parameters (*especially its path*)  <br>
✅ **Add** `Knight` to our level

<br>
### Implement Knight class
{: .highlight-stiple}

<center> Inside <code>Entity.hx</code> 📄 : </center>

- **Add** two new variables in `Entity` class:
```js
Entity {
  /** ... should already has a lot of stuff ... **/
  /** just under public var sprScaleY = 1.0;**/
  /** It will be used to add some juicy effect to our sprites **/
  public var sprSquashX = 1.0;
  public var sprSquashY = 1.0;
}
```

<br>

- **Add** a new function called `setSquashX()` :
```js
  public function setSquashX(v:Float) {
    sprSquashX = v;
    sprSquashY = 2-v;
  }
```

<br>

- **Modify** the already existing function `makePoint()` :
```js
// Will be used to setup point for knight path
/** If you don't, it will bug when coding patrol stuff as there will be a case offset
I couldn't figure out if it was my mistake or if just deepnight code needed this modification
to actually work as expected. In previous game he used Cpoint not Lpoint.**/
public function makePoint() return LPoint.fromCase(cx,cy);
```

<br>

- **Modify** the already existing function `postUpdate()` :
```js
public function postUpdate() {
  spr.x = (cx+xr)*Const.GRID;
  spr.y = (cy+yr)*Const.GRID;
  spr.scaleX = dir*sprScaleX*sprSquashX;
  spr.scaleY = sprScaleY*sprSquashY;
  spr.visible = entityVisible;

  sprSquashX += (1-sprSquashX) * 0.2;
  sprSquashY += (1-sprSquashY) * 0.2;

  /** some stuff**/
}
```

<br>

<center> Inside folder <code>src/en/</code> 📁 : </center>

- **Create** a new class for the `Knight` Entity called `Knight.hx` :

```js
package en;
class Knight extends Entity {
  /** Using an array to get all the instances of a specific Entity is usefull
  especially when you want to delete all of them when a level is completed **/
  public static var ALL : Array<Knight> = [];
  /** The name behind "Entity_" should match what you have written inside LDtk, here "Knight"  **/
  var data : Entity_Knight;

  /** Stuff related to path**/
  var origin : LPoint;
  var patrolTarget : Null<LPoint>;

  public function new(e:Entity_Knight) {
    super(e.cx, e.cy);
    ALL.push(this);

    /** This will allow us, inside functions, to access to the information related to the current Entity.
    These information were defined inside LDtk **/
    data = e;
    /** Thanks to this we get the information related to the initialDir **/
    dir = data.f_initialDir;
    lockControlS(1);

    /** We register the starting position of our knight using a function from Entity.hx **/
    origin = makePoint();

    /** We set the point up to which our knight must patrol **/
    patrolTarget = data.f_patrol==null ? null : LPoint.fromCaseCenter(data.f_patrol.cx, data.f_patrol.cy);

    sprScaleX = rnd(1,1.2);
    spr.anim.setGlobalSpeed(0.2);

    /** Same process as for player. It works because there are the specified frames inside the atlas **/
    spr.anim.registerStateAnim("knightWalk",1, 1, ()->M.fabs(dx)>=0.03);
    spr.anim.registerStateAnim("knightIdle",0);
  }

  override function dispose() {
    super.dispose();
    ALL.remove(this);
  }

  override function postUpdate() {
    super.postUpdate();
  }

  override function update() {
    super.update();
    cd.setS("airControl",0.5);

    if( !controlsLocked() ) {
      var spd = 0.01 * (0.2+0.8*cd.getRatio("airControl"));
      // Fixed patrol
      /** If the following conditions is true, our knight must go right
      ortherwise go left **/
      dir = patrolTarget.levelX > centerX ? 1 : -1;
      dx += spd * dir * tmod;

      /** We reach our patrolTarget so we comeback from where we come **/
      if( cx==patrolTarget.cx && cy==patrolTarget.cy && M.fabs(xr-0.5)<=0.3 ) {
        patrolTarget.cx = patrolTarget.cx==origin.cx ? data.f_patrol.cx : origin.cx;
        patrolTarget.cy = patrolTarget.cy==origin.cy ? data.f_patrol.cy : origin.cy;
        setSquashX(0.85);
        /** We squash a bit the sprite of our knight to add some juice **/
        lockControlS(0.5);
      }
    }
  }
}
```

<br>

<center> Inside <code>Level.hx</code> 📄 : </center>

- **Add** in the function `attachMainEntities()` (*as we did `LDtk/Use LDtk data inside GameBase code`*) :
```js
for( e in level.l_Entities.all_Knight)
    new en.Knight(e);
```
> ⚠️ : Here the name behind *level.l_Entities.all_***[NAME]** is super important, as it should match the name given inside `LDtk`




<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/Knights_game.png"
       width="500">
</figure>

<br>
✅ **Create** a new class for our `Knight` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Get** the animation from atlas <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Modify** the sprite to add some juice <br>
✅ **Instantiate** `Knight` create its instance in the game

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have moving knights everywhere
</div>
