---
title: Multiple levels integration
nav_order: 16
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

<br>


## 📝 What we need to do
{:toc .fs-7}

---

&nbsp;&nbsp;&nbsp; ⬜ **Create** two other levels in `LDtk` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Add** in particular `StairsEndLevel` Entity in each level (*it will serves as a gate between levels*) <br>
&nbsp;&nbsp;&nbsp; ⬜ **Create** a new class for our `StairsEndLevel` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Describe** through code the transition mechanism between level <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Instantiate** it <br>
&nbsp;&nbsp;&nbsp; ⬜ **Make** some simple transitions (*fade In / fade Out*) between levels

<br>

## 💻 Let's do this !
{:toc .fs-7}

---

<br>
### Create levels in LDtk
{: .highlight-stiple}

- **Open** your `LDtk` project

- **Create** a new Entity called `StairsEndLevel`

- **Go** inside the `StairsEndLevel` panel entity

- **Select** `Max count` field and write "1" as there will only be one exit per level

Feel free to create as much levels as you want, and to add in each one, at least one `Hero` & `StairsEndLevel` Entity.

<br>
✅ **Create** two other levels in `LDtk` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Add** in particular `StairsEndLevel` Entity in each level (*it will serves as a gate between levels*)

<br>
### Create class StairsEndLevel
{: .highlight-stiple}

<center> Inside folder <code>src/en/</code> 📁 : </center>

- **Create** a new file `StairsEndLevel.hx` :

```js
package en;

class StairsEndLevel extends Entity {
  var data : Entity_StairsEndLevel;

  public function new(e:Entity_StairsEndLevel) {
    super(e.cx, e.cy);
    gravityMul = 0;
    game.scroller.add(spr, Const.DP_BG);
    spr.set("stair");
  }

  override function postUpdate() {
    super.postUpdate();
  }

  override function dispose() {
    super.dispose();
  }

  override function update() {
    super.update();

  // If hero is on the same case as the door, we activate the win condition
  if (hero.onGround && hero.distCaseX(this)<=1 && hero.cy==cy )
    Game.ME.activateWinCondition();
  }
}
```

<br>
<center> Inside <code>Level.hx</code> 📄 : </center>

- **Instantiate** the Entity at start of `attachMainEntities()`:
```js
var e = level.l_Entities.all_StairsEndLevel[0];
game.stairsEndLevel = new en.StairsEndLevel(e);  
```

> ⚠️ : *If you instantiate it after the `Player` Entity it will be render in
front of the player which could be a bit weird.*

<br>
<center> Inside <code>Const.hx</code> 📄 : </center>

- **Add** a new variable `public static var DARK_COLOR = 0x1b131b;`. <br>
It will be used as the color for our transitions.

<center> Inside <code>Game.hx</code> 📄 : </center>

- **Add** `import en.StairsEndLevel;`.


- **Add** new class variables :
```js
public var stairsEndLevel : StairsEndLevel;
var winCondition = false;
var fadeMask : h2d.Bitmap;
public var lastLevelIdx : Int;
```

<br>

- **Create** three new functions :

```js
public function activateWinCondition(){
  winCondition = true;
}
```

```js
/*We create a tweening alpha effect using our fadeMask.
It goes from opaque to transparency */
function fadeIn() {
  tw.terminateWithoutCallbacks(fadeMask.alpha);
  fadeMask.visible = true;
  tw.createMs( fadeMask.alpha, 1>0, 800, TEaseIn ).end( ()->fadeMask.visible = false );
}

```

```js
/*We create a tweening alpha effect using our fadeMask.
It goes from transparency to opaque */
function fadeOut() {
  tw.terminateWithoutCallbacks(fadeMask.alpha);
  fadeMask.visible = true;
  tw.createMs( fadeMask.alpha, 0>1, 2000, TEaseIn );
}
```

<br>

- **Add** in function `new()`:
```js
fadeMask = new h2d.Bitmap( h2d.Tile.fromColor(Const.DARK_COLOR) );
root.add(fadeMask, Const.DP_TOP);
```

<br>

- **Also add**, just under `startLevel(0)` the following : `lastLevelIdx = world.levels.length - 1 ; `


Logically we want a fadeIn for the beginning of each level.

- **Add**  `fadeIn();` at start of `startLevel()` function  

- **Add** in `update()` function :

```js
// If all the conditions are met we trigger the fade out
if(hero.isAlive() && winCondition && !cd.has("levelComplete")){
  trace("Win");
  cd.setS("levelComplete", Const.INFINITE);
  delayer.addF(function() {
  hero.lockControlS(3);
  }, 10);
  cd.setS("autoNext",2);
  fadeOut();
}

// Once the fade out is over (approx : 2sec) we go to next level.
// If it's last level, we restart it
if( cd.has("levelComplete") && !cd.has("autoNext")){
  winCondition = false;
  trace("MegaWin");
  if(curLevelIdx >=lastLevelIdx)
    curLevelIdx = lastLevelIdx - 1;
  nextLevel();
}
```

<br>

- **Add** `cd.unset("levelComplete");` in `nextLevel()` function

- **Add** in `onResize()` function :
```js
fadeMask.scaleX = w()/fadeMask.tile.width;
fadeMask.scaleY = h()/fadeMask.tile.height;
```

<br>
✅ **Create** a new class for our `StairsEndLevel` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Describe** through code the transition mechanism between level <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Instantiate** it <br>
✅ **Make** some simple transitions (*fade In / fade Out*) between levels

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have the possibility to play multiple levels with transitions between them
</div>
