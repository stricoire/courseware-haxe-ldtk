---
title: UI - Add Life Bar
nav_order: 14
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- What could be a game without a Life Bar, let's see how we can create it using `Haxe` and add some logic to it to make it juicer.
<br>


## 📝 What we need to do
{:toc .fs-7}

---

&nbsp;&nbsp;&nbsp; ⬜ **Create** a new class for our `LifeBar` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Design** it through code <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Define** juicy effect <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Make sure** to position it correctly depending on the window size <br>

&nbsp;&nbsp;&nbsp; ⬜ **Instantiate** it  <br>
&nbsp;&nbsp;&nbsp; ⬜ **Define** a function `hit` to deal with damages  <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Call** it when necessary


<br>

## 💻 Let's do this !
{:toc .fs-7}

---

<br>
### Define LifeBar class
{: .highlight-stiple}

<center> Inside folder <code>src/ui/</code> 📁 : </center>

- **Create** a new *file* `LifeBar.hx` :

```js
package ui;

class LifeBar extends dn.Process {
  public static var ME : LifeBar;

  var barWid = 40;
  var barHei = 5;
  var value = 1.0;
  var bg : h2d.ScaleGrid;
  var bar : h2d.Graphics;
  var cAdd : h3d.Vector;

  public var game(get,never) : Game; inline function get_game() return Game.ME;

  public function new() {
    super(Game.ME);
    ME = this;

    createRootInLayers(game.root, Const.DP_UI);

    cAdd = new h3d.Vector();
    // ScaleGrid is quite interessting as it allow us to render a Tile as a stretchable surface with unscaled corners
    // Here left/right corners are 5 pixels wide; and the same for top/bottom corners
    bg = new h2d.ScaleGrid(Assets.tiles.getTile("uiBox"), 5, 5, root);
    bg.color.setColor( C.addAlphaF( C.interpolateInt(0xE62639, 0xc86e36, 0.) ) );

    bg.width = barWid+32;
    bg.height = 26;

    // Text
    var tf = new h2d.Text(Assets.fontTiny,bg);
    tf.text = "Life";
    tf.textColor = 0xFFB300;
    tf.colorAdd = cAdd;
    tf.setPosition(4,4);

    var bar_wrapper = new h2d.Object(bg);
    bar_wrapper.x = tf.textWidth + 8;
    bar_wrapper.y = 12 - barHei*0.5;

    var bar_outline = new h2d.Graphics(bar_wrapper);
    bar_outline.beginFill(0xFFB300,1);
    bar_outline.drawRect(0,0,barWid+4,barHei+2);

    var bar_bg = new h2d.Graphics(bar_wrapper);
    bar_bg.beginFill(0x371E0F,1);
    bar_bg.drawRect(2,1,barWid,barHei);

    bar = new h2d.Graphics(bar_wrapper);
    bar.beginFill((C.makeColorRgb(bg.color.r, bg.color.g, bg.color.b)),1);
    bar.drawRect(2,1,barWid,barHei);
    bar.colorAdd = cAdd;
  }

  /** Depending on v, our bar will be shorter/longer reflecting the damage you took**/
  public function set(v:Float) {
    value = v;
  }


  override public function onDispose() {
    super.onDispose();
    if( ME==this )
      ME = null;
  }

  override public function postUpdate() {
    super.postUpdate();
    // Juicy effect that makes the bar shake (will be used when hero get damages)
    if( cd.has("shake") )
      root.y += Math.cos(ftime*0.7)*2 * cd.getRatio("shake");

    bar.scaleX += ( M.fclamp(value,0,1) - bar.scaleX ) * 0.4;
  }

  public function blink() {
    cd.setS("shake", 1);
    cAdd.r = 0.9;
    cAdd.g = 0.9;
    cAdd.b = 0.9;
  }

  override public function update() {
    super.update();
    cAdd.r*=0.8;
    cAdd.g*=0.8;
    cAdd.b*=0.8;
  }

  /**Allow us to position correctly our lifebar (here on the top left corner of our window)
   * whatever the size of our windown (native 256x256 or full screen) **/
  override function onResize() {
    super.onResize();
    root.setScale(Const.UI_SCALE);
    root.x =  bg.width*Const.UI_SCALE / 40;
  }
}
```

<br>

<center> Inside <code>Game.hx</code> 📄 : </center>

- **Add** the following in function `new` : `new ui.LifeBar();`

<br>
✅ **Create** a new class for our `LifeBar` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Design** it through code <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Define** juicy effect <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Make sure** to position it correctly depending on the window size <br>

✅ **Instantiate** it


<br>
### Define hit function to deal with damages
{: .highlight-stiple}

<center> Inside <code>Hero.hx</code> 📄 : </center>

- **Add** new class variable:
```js
// This will tell us how many life points the player still have
public var life : Int;
// Here we defined with how many life points the player start with
public var maxLife = 4;
```

<br>

- **Add** at the end of `new` function : `life = maxLife;`


- **Define** a new function `hit()` :
``` js
/* When hit, we decreased life point by damage and we check if player is dead or not
We also make the life bar blink for juice */
public function hit(dmg:Int){
  life-=dmg;
  if( life<=0 ){
    life = 0;
    //Will add stuff concerning restart later on
  }
  ui.LifeBar.ME.blink();
}
```

<br>

- **Call** wherever you want the `hit()` function in function `onLand()`  (*for eg*):
```js
onLand(fallCHei:Float) {
/* stuff */
    if( fallCHei >= 9 ) {
      /* stuff */
      hit(2);
    }
    else if( fallCHei>=3 ){
      /* stuff */
      hit(1);
    }
}
```

<br>

- **Add** function `postUpdate()` with the following (*which will allow us to actualize our life with our current remaining lifePoints*) :
```js
override public function postUpdate() {
  super.postUpdate();
  ui.LifeBar.ME.set(life/maxLife);
}
```

<br>

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/LifeBar.png"
       width="600">
</figure>

<br>

✅ **Define** a function `hit` to deal with damages  <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Call** it when necessary

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have a beautifull Life Bar on the top left of your screen 😊
</div>
