---
title: . . . and jump to make a real platformer
nav_order: 12
parent: Improving Gamebase code
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

- Your player should now be affected by gravity but he still couldn't jump.
That's a bit unfortunate...
<br>


## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Setup** juicy fall effect <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Setup** variables for juicy fall effect <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Add** screen shaking <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Modify** our sprite animation based on the height of the fall <br>
⬜ **Incorporate** jump action inside our moving scheme <br>
⬜ **Improve** character movement <br>


<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Setup juicy fall effect
{: .highlight-stiple}

<center> Inside <code>Entity.hx</code> 📄 : </center>

- **Add** new class variable:
```js
// Will be used to determine the height of a fall and adapt animation accordingly
var fallHighestCy = 0.;
```

<br>

- At the end of **function** `new` add :
```js
  // At start, our highestCy is cy + yr which will be our reference for our fall start
  // It will be actualised if jumping or landing thereafter.
  fallHighestCy = cy+yr;
```

<br>

- **Add** new functions:
```js
function onLand(fallCHei:Float) {
	bdy = 0;
}
public function setSquashY(v:Float) {
  sprSquashX = 2-v;
  sprSquashY = v;
}
```

<br>

- **Add** in `update` function, just under the *commentary line* `// [ add Y collisions checks here ]`:
```js
// If we jump we want to acualise the Y position from which we fall
if( dy<=0 )
    fallHighestCy = cy+yr;
```

<br>

- **Add** in `update` function, inside *condition line* `if( level.hasCollision(cx,cy+1) && yr>=1 )`, at the end of it:
```js
// if we are landing we logically call the onLand function.
// This function will be override in Hero.hx to adjust animation and add juicy effect to player when landing.
onLand(cy+yr-fallHighestCy);
```

<br>
⬜ **Setup** juicy fall effect <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Setup** variables for juicy fall effect <br>

<br>
### Implement juicy onLand function
{: .highlight-stiple}

<center> Inside <code>Hero.hx</code> 📄 : </center>

```js
// If the actual fall is important we will shake the camera and squash hero sprite on landing.
// At the end of the function we reinitialize the value of fallHighestCy
override function onLand(fallCHei:Float) {
    super.onLand(fallCHei);

    var impact = M.fmin(1, fallCHei/6);
    dx *= (1-impact)*0.5;
    game.camera.bump(0, 2*impact);
    setSquashY(1-impact*0.7);

    if( fallCHei>=9 ) {
        lockControlS(0.3);
        game.camera.shakeS(1,0.3);
        cd.setS("heavyLand",0.3);
    }
    else if( fallCHei>=3 )
        lockControlS(0.03*impact);

    fallHighestCy = cy+yr;
}
```

<br>
<br>
✅ **Setup** juicy fall effect <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Add** screen shaking <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Modify** our sprite animation based on the height of the fall <br>

<br>
### Add Jump animation
{: .highlight-stiple}

<center> Inside <code>Hero.hx</code> 📄 : </center>

- **Add** in function `new`, the animation related to jump:
```js
spr.anim.registerStateAnim("heroJumpUp",15, ()->!climbing && !onGround && dy<=0.05 );
spr.anim.registerStateAnim("heroJumpDown",15, 0.9, ()->!climbing && !onGround && dy>0.05 );
spr.anim.registerStateAnim("heroCrouchIdle",0, ()->isCrouching());
```

<br>
- **Add** new functions:
```js
public inline function isCrouching() {
    return isAlive() && ( level.hasCollision(cx,cy-1) && level.hasCollision(cx,cy+1) || cd.has("heavyLand") );
}
```

> ℹ️ : Feel free to experiment with `fallChei` limit conditions to trigger the effect with a smaller or greater fall

<br>
✅ **Incorporate** jump action inside our moving scheme

<br>
### Polish character movement
{: .highlight-stiple}

<center> Inside <code>Hero.hx</code> 📄 : </center>

- **Remove** everything in the `update()` function to replace it with the following :
```js
super.update();
var spd = 0.015;
// We setup some cd(aka cooldown), will be usefull for conditions
if( onGround || climbing ) {
	cd.setS("onGroundRecently",0.1);
	cd.setS("airControl",10);
}
```
```js
// Walk
if( !controlsLocked() && ca.leftDist() > 0 ) {
    var xPow = Math.cos( ca.leftAngle() );
    dx += xPow * ca.leftDist() * spd * ( 0.4+0.6*cd.getRatio("airControl") ) * tmod;
    var old = dir;
    // Update the direction of player
    // -> will allow to reverse the sprite accordingly
    // -> See : Entity.hx -> postUpdate() -> spr.scaleX = dir*sprScaleX;
    dir = M.fabs(xPow)>=0.1 ? M.sign(xPow) : dir;
    if( old!=dir && !climbing )
      spr.anim.playOverlap("heroTurn", 0.66);
}
else
    dx*=Math.pow(0.8,tmod);
```
```js
// Jump
var jumpKeyDown = ca.aDown() || ca.isKeyboardDown(K.Z) || ca.isKeyboardDown(K.W) || ca.isKeyboardDown(K.UP);
var jumpKeyPressed = ca.aPressed()||ca.isKeyboardPressed(K.Z)||ca.isKeyboardPressed(K.W)||ca.isKeyboardPressed(K.UP);
if( !controlsLocked() && jumpKeyPressed && ( !climbing && cd.has("onGroundRecently") || climbing && ca.aDown() ) ) {
    if( !climbing ){
      setSquashX(0.7);
      dy = -0.09;
      cd.setS("jumpForce",0.1);
      cd.setS("jumpExtra",0.1);
    }
}
```
```js
// We allow the player to have a higher or lower jump depending on the duration of the input
else if( cd.has("jumpExtra") && jumpKeyDown )
    dy-=0.04*tmod;
if( cd.has("jumpForce") && jumpKeyDown )
    dy -= 0.05 * cd.getRatio("jumpForce") * tmod;
```
```js
if( onGround || dy<0 )
    cd.setS("fallSquash", 1);
/* Add compression effect to sprite when falling
Could increase the '0.1' value to increase the effect
cd.getRatio("fallSquash") equal 1 at start of cooldown and 0 when
it ends */
if( !onGround && dy>0 )
    setSquashX( 1 - 0.1 * (1-cd.getRatio("fallSquash")) );
```

> ℹ️ : More information on the cooldown [here](https://github.com/deepnight/deepnightLibs/blob/master/src/dn/Cooldown.hx)

<br>
✅ **Improve** character movement

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have jumping capabilities and juicy effects associate with it !
</div>
