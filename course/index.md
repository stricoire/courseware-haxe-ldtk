---
title: Home
nav_order: 1
description: "Courseware for Haxe and LDtk is a static website to learn the basics of Haxe and LDtk"
permalink: /
---

# Courseware for Haxe and LDtk
{: .fs-9 }

Welcome to the Courseware for Haxe and LDtk!
This is a web tutorial to learn the basics for Haxe and LDtk.

This project started out as a fork of courseware-template [(click here for the gitlab template)](https://gitlab.com/courseware-as-code/courseware-template).
{: .fs-6 .fw-300 }

[Get started now](https://stricoire.gitlab.io/courseware-haxe-ldtk/course/I-Installation/Installation/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [About the project](#about-the-project){: .btn .fs-5 .mb-4 .mb-md-0 }

---

## About the project

Hello my name is Simon 👋, <br>
I'm spare time game developer, known as LumbirBwut on [Itchio](https://lumbirbwut.itch.io/).


The purpose of this series of paper tutorials is to provide first of all more explanations, in order to better apprehend the development of games via Haxe and what revolves around it.

In a second time to propose a simple and didactic way to use LDtk with our Haxe projects.

Finally in a last time, based on jams games realized by the developer [deepnight](https://deepnight.net/), to extract and understand the structure of his code, to be able to reproduce in the long run similar functionalities in our own projects (*to manage the animations of our entities, to implement a management of the collisions, to create the typical movements of a 2D platformer, to add text, etc.*).

### Special Thanks

Big thanks to `deepnight` taking the time with GameBase and LDtk, it helps a lot ! 👍

### Disclaimer

I'm a junior programmer, who relies mainly on his academic background to deconstruct the code, and propose functional code snippets.
Please, keep in mind though that it is possible that I made mistakes or that I proposed not fully optimized solutions. Feel free to tell me so, and I'll try to correct my mystakes !
