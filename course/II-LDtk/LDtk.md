---
title: LDtk
nav_order: 3
has_children: true
---

## `LDtk` Installation  
{: .fs-7}

---

- **Download** the software [here](https://ldtk.io/)

> ℹ️ : `LDtk`is an open-source 2D level editor from the creator of Dead Cells,
with a strong focus on user-friendliness.

<br>
- **Type** the following command to download `ldtk-haxe-api`:

```bash
haxelib install ldtk-haxe-api
```

> ℹ️ : More info [here](https://github.com/deepnight/ldtk-haxe-api)

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have <code>LDtk</code> installed
</div>
