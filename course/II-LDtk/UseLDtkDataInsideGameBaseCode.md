---
title: Use LDtk data inside GameBase code
nav_order: 6
parent: LDtk
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Get** reference to our tileset <br>
⬜ **Load** entities from our tileset <br>
⬜ **Render** our level based on layout `Collisions` we defined in `LDtk` <br>
⬜ **Define** a new class for our `Hero` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Get** the animation from atlas <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; • **Implement** simple controls scheme <br>
⬜ **Create** functions to load specific levels based on index <br>
⬜ **Make** camera follows the `Hero`
<br>
<br>
<br>

## 💻 Let's do this !
{:toc .fs-7}

---

### Load informations related to our tileset
{: .highlight-stiple}

<center> Inside <code>Level.hx</code> 📄 : </center>

- **Add** in `Level` class:

```js
Level extends dn.Process {
  /** ... should already has stuff ... **/

  public var level : World_Level;
  /** Serves as a virtual container to position our layers related to one another**/
  var lightWrapper : h2d.Object;

  var bgColor : h2d.Graphics;
  var walls : h2d.TileGroup;
}
```

<br>
- **Add** in function `new`:

```js
level = l;
/** Here we link the tileset we used to draw our level  **/
/** trace(level.l_Collisions.tileset); RETURN logically "ldtk.Tileset[#Tileset,path=tileset.png]" **/
var sourceTile = l.l_Collisions.tileset.getAtlasTile();
lightWrapper = new h2d.Object(root);
bgColor = new h2d.Graphics(lightWrapper);

/** Defining a TileGroup (static Tile batch renderer) will allow us to render our tiles**/
/** Here as both bgColor and walls inherit from lightWrapper, render order will be linked
to the order of creation. If you create walls before bgColor it will be hide by the latter**/
walls = new h2d.TileGroup(sourceTile, lightWrapper);
```


and don't forget, `new` should have as parameter, the following :
```js
new (l : World_Level)
```

> ℹ️ : If you use VScode, the autocompletion should propose it to you

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/autoCompleteWorldLevel.png"
       width="500">
</figure>

<br>

- **Create** a new function called `attachMainEntities` <br>
(*It will be then call by `Game.hx` each time we want to load the entities associated with a specific level*) :

```js
public function attachMainEntities() {
  var e = level.l_Entities.all_Hero[0];
  game.hero = new en.Hero(e);
}
```

> ℹ️ : You may have noticed, that it's actually pretty simple to get the information from LDtk using the ldtk-api for Haxe.
Usually, you just have to have access to you World_Level object to then call layers, entities, etc from LDtk.
More info [here](https://ldtk.io/docs/game-dev/haxe-in-game-api/usage/)

> 📖 Example 📖 : to access other entities in your function `attachMainEntities` just add the following :
(*supposedly having defined an entity called `Entity2` in LDtk*)

```js
for(e in level.l_Entities.all_Entity2)
  new en.Entity2(e);
```
> ⚠️ : Don't forget to create the class related to `Entity2` in your code

<br>
✅ **Get** reference to our tileset <br>
✅ **Load** entities from our tileset


<br>
### Render our level based on Collisions layout
{: .highlight-stiple}

<center> Inside <code>Level.hx</code> 📄 : </center>

- **Remove everything** in function `render`

- **Add** the following instead :

```js
walls.clear();
bgColor.clear();

/** For the background color, we just draw a rectangle of the size of our level with the bgColor we specified in LDtk **/
bgColor.beginFill(level.bgColor);
bgColor.drawRect(0,0,512, 512);

/**  Walls : here we iterate through all the tiles that we draw previously in LDtk
and render it based on their position **/
for (autoTile in level.l_Collisions.autoTiles){
  var tile = level.l_Collisions.tileset.getAutoLayerTile(autoTile);
  walls.add(autoTile.renderX, autoTile.renderY, tile);
}
```

> ℹ️ : You could define the background color using another method. <br>
Just go inside `Main.hx`, inside `new` function, change the value of `engine.backgroundColor = 0xff<<24|0x`+*yourColorInHexadecimal*

<br>
✅ **Render** our level based on layout `Collisions` we defined in `LDtk`


<br>
### Create Hero class
{: .highlight-stiple}


<center> Inside folder <code>./src</code> 📁 : </center>

- **Create** the folder `./en` (*it will contains all the classes related to Entities*)

- **Create** inside it, the file `./en/Hero.hx`

- **Add** the following :

```js
package en;

import h2d.Tile;
import h2d.Bitmap;
import hxd.Res;

class Hero extends Entity {
  var ca : ControllerAccess;
  var data : Entity_Hero;

  public function new(e:Entity_Hero) {
    super(e.cx, e.cy);
    data = e;

    Game.ME.scroller.add(spr, Const.DP_BG);

    /** Animation related stuff **/
    /** What the following function does is :
    You specify the base name of your animation, here "heroRun",
    It will then look inside the atlas you previously created for the corresponding assets.
    The function also want the priority of the animation, its speed and a trigger condtion.
    **/
    spr.anim.registerStateAnim("heroRun",5, 0.08, ()->M.fabs(dx)>=0.05 );
    spr.anim.registerStateAnim("heroIdle",0, 0.1, ()->true);

    ca = Main.ME.controller.createAccess("hero"); // creates an instance of controller
  }

  override function dispose() { // call on garbage collection
    super.dispose();
    ca.dispose(); // release on destruction
  }

  override function update() { // the Entity main loop
    super.update();

    /** Controller related stuff **/
    /** We move hero entity based on input player coming from controller or keyboard**/
      if( ca.leftDown() || ca.isKeyboardDown(hxd.Key.LEFT) )
         dx -= 0.025*tmod;
      if( ca.rightDown() || ca.isKeyboardDown(hxd.Key.RIGHT) )
         dx += 0.025*tmod;
      if( ca.upDown()|| ca.isKeyboardDown(hxd.Key.UP) )
         dy -= 0.025*tmod;
      if( ca.downDown() || ca.isKeyboardDown(hxd.Key.DOWN) )
         dy += 0.025*tmod;
  }
}
```

<br>
✅ **Define** a new class for our `Hero` <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Get** the animation from atlas <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ✅ **Implement** simple controls scheme


<br>
### Load levels and track Hero  
{: .highlight-stiple}

<center> Inside <code>Game.hx</code> 📄 : </center>

- **Add** : `import en.Hero;`

- **Add** in `Game` class :
```js
Game extends dn.Process {
  /** ... should already has stuff ... **/
  public var hero : Hero;

  /** LDtk world data **/
  public var world : World;
  public var curLevelIdx = 0;
}
```

<br>
- **Add** in function `new` :
```js
/** just under : hud = new ui.Hud(); **/
world = new World();
startLevel(0);
camera = new Camera();
/** We track the hero entity with the camera (not mandatory though)**/
camera.trackEntity(hero, true);
```

<br>
- **Remove**, *still in function `new`*, the two lines above `hud = new ui.Hud();` concerning `level` and `camera`


- **Create** two new functions named `startLevel` and `nextLevel`.
This two functions will allow us to load specified level (*corresponding to the ones defined in LDtk*) :

```js
function startLevel(idx=-1, ?data:World_Level) {
  curLevelIdx = idx;
  // Cleanup
  if( level!=null )
    level.destroy();
  for(e in Entity.ALL)
    e.destroy();
  fx.clear();
  gc();
  // End game
  if( data==null && idx>=world.levels.length ) {
    destroy();
    trace("END GAME");
    return;
  }
  /** Init : we call the function we previously defined in Level.hx **/
  level = new Level( data!=null ? data : world.levels[curLevelIdx] );
  level.attachMainEntities();
  Process.resizeAll();
  hxd.Timer.skip();
}

/** This will be usefull in the near futur when game mechanics will be implemented.
For eg: We detect that the actual level is over, we want to go to the next one,
we just have to call this function  **/
public function nextLevel() {
  startLevel(curLevelIdx+1);
  /** We track the hero entity with the camera (not mandatory though)**/
  camera.trackEntity(hero, true);
}
```
<br>
✅ **Create** functions to load specific levels based on index <br>
✅ **Make** camera follows the `Hero`

<br>
### Adjust screen constants
{: .highlight-stiple}

<center> Inside <code>Const.hx</code> 📄 : </center>

- **Modify** the value of variable `GRID` to match your tiles size (*for me it's 16*)
- **Modify**, *inside `SCALE` function*, the value to best match wanted resolution :

```js
// for eg : 256 x 256 as it's half size of my level, and I want my view
// to be a little zoomed in
return dn.heaps.Scaler.bestFit_i(256,256);
```

<br>
<center> Inside the <code>game root folder</code> 📁 : </center>

- **Open** a `CMD`

- **Enter** the following command to compile and run your project (*or if you use VSCode just press `F5`*) :

```bash
haxe hl.dx.hxml
hl ./bin/client.hl
```

<br>
### Optionnal) Information related to Resolution Setup
{: .highlight-stiple .text-align-right}

- Inside `Camera.hx`, you could set `clampToLevelBounds` to `false` if you want your camera to not be block by the limits of your level while tracking an **Entity**.

- Inside `hl.hxml` it is possible to specify the windowSize using this command : `-D windowSize=512x512`

- Calling `h()` or `w()` **functions** will return height/width of window

- Inside `new` **function** of `Main.hx` it is possible to allow or not the full screen (*by default it's activated*) :         `engine.fullScreen = true;`

- Understanding relation between `LDtk` level size, **windowSize**, and the `Scale` value outputed by `bestFit_i` function could be a bit tricky, so let me try to wrap things up about what i've learned (*keep in mind that it may be subjected to errors*) :
  - So `LDtk` level size will be basically your playing area, it could be important and vary depending on levels as the camera can follow  the player along the way (*if you want to*)(*for eg: it could be 1024 pixels wide and 512 pixels height*)

  - **windowSize**, by default it will be fullScreen if you don't desactivate the option inside `Main.hx`; once it's done you could give it the value you want as long as it respects the maximum values of your monitors. However keep in mind, that this value will influence the output of `Scale` variable (*for eg: let's decide that our game will be windowed with 512 pixels wide and 512 pixels height*)

  - Finally the `Scale` value outputed by `bestFit_i` **function**, <br>
  It's use in lot of places but what does it does and how it works? <br><br>
  So from what I've understand `bestFit_i` **function** will calculate an **Int ratio** based on the values you give as parameter to the function and the value of the **windowSize**.
  More specifically it will divide the smaller value between width/heigh of **windowSize** and divide it by the higher value between the two parameters of `bestFit_i` **function** (*for eg: let's decide that you give* `bestFit_i` **function** *(256,256) as parameters, the output/valueOfScale will be 512/256 = 2*).
  So now you have your `Scale` value it will render your level based on it and adapt pixels accordingly (*for eg: Your `Scale` value is 2 means that for one pixel in `LDtk` you will have a square of 2x2 with the same value in your window instead*).

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have successfully rendered (at least) one level from <code>LDtk</code> in your game
and you also have a basic/animated moving character as a player.
</div>
