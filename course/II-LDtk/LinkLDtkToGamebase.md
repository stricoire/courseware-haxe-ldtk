---
title: Link LDtk to gamebase project
nav_order: 5
parent: LDtk
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


- As you may have noticed, `gameBase` isn't directly working with `LDtk`, and therefore in order to make it work, several steps are necessary beforehand.

- Those steps are described in general terms [here](https://ldtk.io/docs/game-dev/haxe-in-game-api/usage/).
However, I found more clarity during my research, to lean on the way `deepnight` (*he is the developer of `LDtk` and gameBase*) structured it for one of his [jam projects](https://github.com/deepnight/ld47-fortLoop/tree/master/src)

*The purpose of the following is to modify gameBase little by little to make it work with `LDtk`*

## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Refer** to `LDtk` through code using ldtk/haxe api <br>
⬜ **Add** `LDtk` project and our atlas to the gamebase <br>
⬜ **Link** our `LDtk` project (*to be able to access it through code later on*) <br>


## 💻 Let's do this !
{:toc .fs-7}

---

### Create reference to LDtk
{: .highlight-stiple}

- **Add** the following line of code to the compilation file `base.hxml` (*as above, I'm using `Hashlink` for running the game, you may have to add this to another compilation file, if you don't*) :

```bash
-lib ldtk-haxe-api
```

<br>
✅ **Refer** to `LDtk` through code using ldtk/haxe api

<br>
### Add ressources files to project
{: .highlight-stiple}

- **Create** a new folder `res\world` (*it will contains all the data related to `LDtk`*). <br>

- **Put inside** the folder, the project `world.ldtk` with the tileset you previously created (*or [tileset.png]({{site.baseurl}}/attached_files/files/tileset.png) given with this tutorial*).

- **Replace** all the existing files inside `res\atlas`, by the ones I give in this tutorial:
respectively  [`tiles.atlas`]({{site.baseurl}}/attached_files/files/tiles.atlas) and [`tiles.png`]({{site.baseurl}}/attached_files/files/tiles.png) (*which are the ones used by `deepnight` dev for its gameJam project*).

> ℹ️ : If you want more information concerning atlas (*what is it? how to generate it? etc*), check [this section](#optionnal-information-related-to-atlas-) down below about it.

<br>
✅ **Add** `LDtk` project and our atlas to the gamebase

<br>
### Grant access through code to LDtk project
{: .highlight-stiple}

- **Create** a new file named `src\World.hx`. <br>
This file will host all the typed data extracted from the project. <br>

- **Add** the following line of code inside `World.hx` :
```js
private typedef _Tmp =
    haxe.macro.MacroType<[ ldtk.Project.build("res/world/world.ldtk") ]>;
```

- **Add** the following line of code inside `src\import.hx` (*for better code completion and for more convenience later*) :
```js
import World;
```

- **Check** inside `src\Assets.hx` if this line : <br>
 `tiles = dn.heaps.assets.Atlas.load("atlas/tiles.atlas");` reference the atlas you previously created (*aka: got the same name*).

 <br>
✅ **Link** our `LDtk` project (*to be able to access it through code later on*)

<br>
### Optionnal Information related to Atlas <a name="AtlasInformation"></a>
{: .highlight-stiple .text-align-right}

- A **Tileset** is simply a collection of single tiles grouped together.

- A **Tilemap** uses a grid of tiles (**Tileset**) to create a game map.

- To me an **Atlas**  is quite similar to a **Tileset** as it's also a collection of single tiles grouped together. I think the difference may lie on the fact that an atlas can gather unrelated stuff and in particular the frames of different animation.



- In this tutorial [`tiles.png`]({{site.baseurl}}/attached_files/files/tiles.png) contains all the different frames, and [`tiles.atlas`]({{site.baseurl}}/attached_files/files/tiles.atlas) contains all the informations related to each frame (***its name and position***) which is super usefull because you just have to properly mention a frame in the code to render it.

<br>
#### From this, come the main question : How to generate the `tile.atlas` we need?
<br>

After doing somes research here is the answer :
> 👍 to `Gamefromsratch` and [his video](https://www.youtube.com/watch?v=B-F6splWNGI)

**Solution 1)**
You use `Texture packer` from [`libgdx`](https://github.com/libgdx/libgdx/wiki/Texture-packer)
but you may have to dig in some code to get it working.

**Solution 2)** (*the one I've chosen*)
You could use the **software** [`Texture Packer`](https://www.codeandweb.com/texturepacker). <br>
There is a free version and it's quite easy to use.

- **Drag & Drop** all the sprites you want in your **Atlas** (*it should be looking like this*) :

<figure>
  <img src="{{site.baseurl}}/attached_files/images/sprites_TexturePacker.png"
       width="500">
</figure>

<br>
- **Change** the `Data Format` to `LibGDX`.

<figure>
  <img src="{{site.baseurl}}/attached_files/images/libGDX_TexturePacker.png"
       width="500">
</figure>

<br>
- **Click** on `Publish sprite sheet`, *Et voilà* 🎉

<figure>
  <img src="{{site.baseurl}}/attached_files/images/publish_TexturePacker.png"
       width="500">
</figure>

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should have setup all the necessary ressources to use <code>LDtk</code> with your project
</div>
