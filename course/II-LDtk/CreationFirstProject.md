---
title: Create First Project
nav_order: 4
parent: LDtk
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## 📝 What we need to do
{:toc .fs-7}

---

⬜ **Create** a new `LDtk` project <br>
⬜ **Draw** our levels using `LDtk` system rules and our imported tileset <br>
⬜ **Create** a new `LDtk` Entity `Hero` <br>
⬜ **Add** `Hero` to our level
<br>
<br>

> ℹ️ :
- You could totally use the one I link with the project [here]({{site.baseurl}}/attached_files/files/world.ldtk)  and skip this part but you'll miss some explanation on how things work.
- I recommend you to at least glance over the following `LDtk` [documentation](https://ldtk.io/docs/general/editor-components/), as it's concise and give good explanations about how the software works.  
<br>



## 💻 Let's do this !
{:toc .fs-7}

---

### Starting LDtk
{: .highlight-stiple}

- **Create or download** a tileset which will be used to create level ground.
<br> (*Personaly, for this tutorial I just [downloaded a tileset](https://opengameart.org/content/a-platformer-in-the-forest), 👍 to `Buch` for his/her contribution*).

- **Launch** `LDtk`

- **Create** a new `world.ldtk` project

- **Add** the previously created tileset using `Tilesets` tab (*or by pressing `F6`*)

> ⚠️ : Here the tiles are 16x16 pixels, but according to the project, it may differs, so if you have to change it don't forget to do so when importing the tileset inside `LDtk`, via the `Layout` entry,

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/layout_LDtk.png"
       width="500">
</figure>

<br>
✅ **Create** a new `LDtk` project

<br>
### Define collisions rules and level design
{: .highlight-stiple}

- **Open** `Layers` tab (*or by pressing `F3`*)

- **Create** a new `Integer Grid` layer named `Collisions`

- **Name** the first `Grid values` entry as `wall`

> ⚠️ : You could name it the way you want, but keep in mind that it will be called via code later on, based on its name. Also, don't forget to specify the previously created tileset via the `Auto-layer-tileset` entry.

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/collisionsLayer_LDtk.png"
       width="500">
</figure>


> ℹ️ : You may have noticed that there is an int value equal to 1 associated with `wall`.
Keep that in mind as it will be useful to detect collisions via code.

> ℹ️ : The following is the one from `deepnight` gameJam project. As `walls` and `ladder` should be treated differently in terms of collisions, the fact that they have different int value associated with, will be super useful later on.

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/collisionsLayer_deepnight_LDtk.png"
       width="500">
</figure>

```js
/** Here is an example of how to refer to Collisions layer inside of code **/
/** and to detect if for the given case there is actually a wall ("==1") **/
level.l_Collisions.getInt(cx,cy)==1
```

- **Define** some rules for `Collisions` layer

- **Draw** your level

> ℹ️ : If you struggle with rules, have a look to this [tutorial](https://ldtk.io/docs/tutorials/auto-layers/auto-layer-rules/) as there are explanations on how to do it.

> ℹ️ : My level is 512 x 512 px, you can modify this by going to `Current Level` tab (*or by pressing `F2`*)

<br>
✅ **Draw** our levels using `LDtk` system rules and our imported tileset

<br>
### Add hero
{: .highlight-stiple}

- **Create** a new `Entity` named `Hero`

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/HeroEntity_LDtk.png"
       width="500">
</figure>

- **Create** a new `Entities layer` named `Entities`. <br>
You should have something looking like this :

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/HeroEntity2_LDtk.png"
       width="500">
</figure>

- **Place** your hero wherever you want inside your level !

<br>
✅ **Create** a new `LDtk` Entity `Hero` <br>
✅ **Add** `Hero` to our level


<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have design at least one level in <code>LDtk</code> and one <code>Hero</code> Entity
</div>
