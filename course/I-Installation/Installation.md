---
title: Installation
nav_order: 2
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


## `Haxe` + `Heaps` Installation
{:toc .fs-7}

---

-  **Follow** the first part of this [tutorial](https://deepnight.net/tutorial/a-quick-guide-to-installing-haxe/), by `deepnight`, as it's complete and quite crystal clear (*in theory even for Windows there should be no problems*).

> ℹ️ : `Haxe` is an open source high-level strictly-typed programming language with a fast optimizing cross-compiler.
`Heaps` is a mature cross platform graphics engine designed for high performance games. It is designed to leverage modern GPUs that are commonly available on both desktop and mobile devices.



## `Hashlink` Installation
{:toc .fs-7}

---

- **Watch** this [video](https://www.youtube.com/watch?v=2oWHTJzYlPs), in addition of the previous links to setup `Hashlink` (*`Haxe` virtual machine*), as it's a bit tricky for windows.


> ⚠️ : Windows CMD is a bit fussy especially regarding PATH variable, to have `haxe` and `hl` command work everywhere. Personally, sometimes after restarting my computer, I needed to go check PATH variable to get it to work (*even just looking inside it, seems for windows sufficient to recognize it again*)

<br>
- **Make sure** you have something like this in your `System variable` :

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/Path.png"
       width="500">
</figure>

<br>

- **Check** that your `PATH` variable reference `Haxe` and `Hashlink` :

<figure class="image-center">
  <img src="{{site.baseurl}}/attached_files/images/Path2.png"
       width="500">
</figure>

## Verify the installation using gameBase by `deepnight`
{:toc .fs-7}

---

- **Follow** this [tutorial](https://deepnight.net/tutorial/using-my-gamebase-to-create-a-heaps-game/)

### Highlights of the tutorial

- **Download** the following [Haxe Project](https://github.com/deepnight/gameBase)
- **Install** dependencies : `deepnightLibs` and `CastleDB` using the followings commands :

```bash
haxelib git deepnightLibs https://github.com/deepnight/deepnightLibs.git
haxelib git castle https://github.com/ncannasse/castle.git
```

> ℹ️ :
- `CastleDB` is a simple database for Haxe games, see [castledb.org](castledb.org)
- `deepnightLibs` regroup several classes developped and used by `deepnight` developer.

<br>
- **Compile** the project for Hashlink Virtual Machine using the following command in gameBase root folder :
```bash
haxe hl.dx.hxml
```

> ⚠️ : This should have generated a `client.hl` in your `\bin` folder

<br>
- **Run** the project using Hashlink :
```bash
hl bin\client.hl
```

> ⚠️ : You could directly build with VScode by pressing F5.

<br>
### Additionnal precisions

#### HXML Files

- Files with `.hxml` extension are compilation files. In those files we typically link the library needed, specify what is the main file, etc.

👇 `(example below)` 👇

<figure>
  <img src="{{site.baseurl}}/attached_files/images/FileHxml.png"
       width="230">
</figure>

> ℹ️ :
- `-lib castle` link to a [library](http://castledb.org/) used for databasing purpose.
- `-lib heaps` link to the [cross platform graphic engine](https://heaps.io/)
- `-lib deepnightLibs` link to [all the libs](https://github.com/deepnight/deepnightLibs) which are used by deepnight developer (*it's needed to compile his project*)
- `-lib ldtk-haxe-api` link to [an api](https://github.com/deepnight/ldtk-haxe-api) that allow to load LDtk Project JSON files in your project (*LDtk being an Open Source 2D level editor*)
- Adding `-hl bin/client.hl` allow you to specify where the executable will be generated




#### import.hx File

This is a special filename, its name should be as it is, to work properly. This feature allows us to define `imports` and `usings` that will be applied for all modules (files) inside a directory, which is particularly handy for large code bases with a lot of helpers and static extensions. (*see more info [HERE](https://haxe.org/blog/importhx-intro/)*)

<br>
<div style="text-align:center; font-size:2em">
  🎉 Congrats this part is now done 🎉
</div>
<div style="text-align:center">
You should now have <code>Haxe</code>, <code>Heaps</code> and <code>Hashlink</code> installed
</div>
