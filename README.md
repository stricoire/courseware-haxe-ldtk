# Courseware for Haxe and LDtk

This repository contains a static website deployed on Gitlab Pages, to learn the basics of Haxe and LDtk.

This project started out as a fork of the amazing courseware-template [(click here for the gitlab template)](https://gitlab.com/courseware-as-code/courseware-template).

Courseware for Haxe and LDtk wouldn't have been possible without it.

[See it in action!](https://stricoire.gitlab.io/courseware-haxe-ldtk/)
